# init.lua para Windows 11
Este é o meu arquivo de configuração do NeoVim para Windows 11.

## Pré-requisitos
Usei neste setup [Scoop](https://scoop.sh) para fazer a instalação do:
- Nvim 0.9.1
- gcc 11.2.0
- ripgrep 13.0.0
- node 18.17.0


## Instalação do Packer
O gerenciador de pacotes que utilizo é o [Packer](https://github.com/wbthomason/packer.nvim).
A instalação é bastante simples, bastando clonar o repositório e criar uma entrada no init.lua ou semelhante:

```shell
git clone https://github.com/wbthomason/packer.nvim "$env:LOCALAPPDATA\nvim-data\site\pack\packer\start\packer.nvim"
```

A segunda parte, como usar chamar o Packer no nvim, eu discuto mais a frente.


## Estrutura dos diretórios
No Windows os arquivos de configuração do nvim devem ficar no path:

```shell
cd $env:LOCALAPPDATA\Local\nvim
```
Caso a pasta não exista é só criar.

A estrutura de diretórios propostas é a seguinte:

```
raiz windows: $env:LOCALAPPDATA\Local\
raiz no mac:  $HOME\.config 

--- 

nvim
|-init.lua
|-after
  |-plugin
    |-lsp.lua
    |-prettier.lua
    |-telescope.lua
    |-treesitter.lua
    |-undotree.lua
|-lua
  |-thiago
    |-colorscheme.lua
    |-init.lua
    |-plugins.lua
    |-remap.lua
    |-set.lua
```

Assim:
- A pasta nvim contém o arquivo de inicialização (init.lua) que é carregado no momento que abrimos o editor
    - Este arquivo faz referência ao init da pasta thiago, onde os demais componentes estão organizados.  
- A pasta afert/plugin server para rodar as configurações especificas de cada plugin instalado, se necessário, ficando mais organizado
- A pasta lua/thiago é onde ficam:
    - init.lua: o inicializador das configurações
    - colorscheme.lua: tem as configurações para cor e transparência
    - plugins.lua: arquivo que define quais plugins serão administrados pelo Packer
    - remap.lua: arquivo com os modificadores de teclas de atalho
    - set.lua: configurações globais do nvim
